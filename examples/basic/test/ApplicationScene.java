package basic.test;
import org.mt4j.*;
import org.mt4j.util.*;
import org.mt4j.util.math.Vector3D;
import org.mt4j.input.inputProcessors.globalProcessors.CursorTracer;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.components.MTComponent;
import org.mt4j.components.visibleComponents.font.FontManager;
import org.mt4j.components.visibleComponents.widgets.*;

import processing.core.PFont;

import javax.swing.*;
public class ApplicationScene extends AbstractScene{

	private MTApplication app;
	public ApplicationScene(MTApplication mtApplication, String name) {
		super(mtApplication, name);
		this.app=mtApplication;
		// TODO Auto-generated constructor stub
		this.registerGlobalInputProcessor(new CursorTracer(mtApplication, this));
		MTTextArea title=new MTTextArea(app);
		title.setFont(FontManager.getInstance().createFont(mtApplication, "Century Schoolbook L Bold", 100, MTColor.GRAY,MTColor.GRAY,false));
		title.setNoStroke(true);
		title.setNoFill(true);
		this.getCanvas().addChild(title);
		title.setText("IMReal");
		this.addLoginBox();
		this.getCanvas().updateComponent(0);
		for (String fontName : PFont.list()) {
			System.out.println(fontName);
		}
	}
	
	public void addLoginBox()
	{
		double loginwindow_width=app.width/2.62;
		double loginwindow_height=app.height/2.62;
		double loginwindow_x=(app.width-loginwindow_width)/2;
		double loginwindow_y=(app.height-loginwindow_height)/2;
		MTWindow loginscene=new MTWindow((int)loginwindow_x,(int)loginwindow_y,0,(int)loginwindow_width,(int)loginwindow_height,10,10,this.app);
		loginscene.setPickable(false);
		loginscene.removeAllChildren();
		loginscene.setFillColor(MTColor.WHITE);
		MTTextArea login_title=new MTTextArea((float)loginwindow_x+5,(float)loginwindow_y+5,(float)loginwindow_width-10,(float)loginwindow_height/5,this.app);
		//login_title.setFillColor(MTColor.GREY);
		login_title.setFont(FontManager.getInstance().createFont(app,"arial.ttf",40,MTColor.GRAY,MTColor.GRAY));
		login_title.setText("Login");
		login_title.setPickable(false);
		login_title.setSelected(true);
		MTTextArea username=new MTTextArea((float)loginwindow_x+5,(float)(loginwindow_y+(loginwindow_height/5)+10),(float)loginwindow_width/2-10,(float)loginwindow_height/5,this.app);
		username.setText("Username");
		MTTextArea password=new MTTextArea((float)loginwindow_x+5,(float)(loginwindow_y+(2*loginwindow_height/5)+20),(float)loginwindow_width/2-10,(float)loginwindow_height/5,this.app);
		password.setText("Password");
		MTTextField username_field=new MTTextField((float)(loginwindow_x+(loginwindow_width/2)),(float)(loginwindow_y+(loginwindow_height/5)+10),(float)loginwindow_width/2-10,(float)loginwindow_height/5,FontManager.getInstance().createFont(app,"DEFAULT_FONT",20),this.app);
		MTTextField password_field=new MTTextField((float)(loginwindow_x+(loginwindow_width/2)),(float)(loginwindow_y+(2*loginwindow_height/5)+10),(float)loginwindow_width/2-10,(float)loginwindow_height/5,FontManager.getInstance().createFont(app,"DEFAULT_FONT",20),this.app);
		//login_title.se
		username_field.setFillColor(MTColor.GRAY);
		password_field.setFillColor(MTColor.GRAY);
		username_field.setText("Dhruv");
		password_field.setText("dhruvkp");
		username_field.setEnabled(true);
		password_field.setEnabled(true);
		username_field.setEnableCaret(true);
		password_field.setEnableCaret(true);
		loginscene.addChild(login_title);
		loginscene.addChild(username);
		loginscene.addChild(password);
		loginscene.addChild(username_field);
		loginscene.addChild(password_field);
		//this.getCanvas().addChild(login_title);
		for(MTComponent mtc : loginscene.getChildren())
		{
			mtc.setPickable(false);
		}
		this.getCanvas().addChild(loginscene);
		/*try
		{
			JWindow loginwindow = new JWindow((JFrame) null);
			loginwindow.setVisible(true);
			loginwindow.setBounds((int) loginwindow_x, (int) loginwindow_y,
					(int) loginwindow_width, (int) loginwindow_height);
			JLabel username_label = new JLabel();
			username_label.setText("Username");
			JLabel password_label = new JLabel();
			password_label.setText("Password");
			JTextField username_field = new JTextField(15);
			JPasswordField password_field = new JPasswordField(15);
			loginwindow.add(username_label);
			loginwindow.add(password_label);
			loginwindow.add(username_field);
			loginwindow.add(password_field);
			loginwindow.setVisible(true);
			SwingTextureRenderer loginbox_renderer = new SwingTextureRenderer(app, loginwindow);
			loginbox_renderer.scheduleRefresh();
		}
		catch(Exception e)
		{
			System.out.print(e.getMessage());
		}*/
	}
	public void init() {}
	public void shutDown() {}
	
}
