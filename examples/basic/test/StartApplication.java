package basic.test;
import org.mt4j.MTApplication;
import processing.core.*;
import basic.helloWorld.HelloWorldScene;


public class StartApplication extends MTApplication {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		initialize();
	}
	
	public void startUp() {
		addScene(new ApplicationScene(this, "First Scene"));
	}

}
